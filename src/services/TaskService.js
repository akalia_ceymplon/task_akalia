import {API_URL} from '../api';

export const TaskService = {
    index,
    create,
};
var accessToken = localStorage.getItem('uid');
function index() {
    const requestOptions = {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + accessToken,
          'Content-Type': 'application/json'
        },
    };
    let getUrl = API_URL + 'api/tasks';
   return fetch(getUrl, requestOptions).then((response) => response.json())
       .then((responseJson) => {
           return Promise.resolve(responseJson);
       })
       .catch((error) => {
           return Promise.reject(error);
       });
}

function create(payload) {
    const requestOptions = {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + accessToken,
          'Content-Type': 'application/json'
        },
        body:payload,
    };
    let createUrl = API_URL + 'api/tasks';
   return fetch(createUrl, requestOptions).then((response) => response.json())
       .then((responseJson) => {
           return Promise.resolve(responseJson);
       })
       .catch((error) => {
           return Promise.reject(error);
       });
}


