import React from 'react';
import {Router, Route} from 'react-router-dom';
import {history} from './helpers/index';
import {LoginRoute} from './component/Route';
import Login from './component/Auth/index';
import Todo from './component/Todo/index';

class App extends React.Component {
    constructor(props) {
        super(props);
        history.listen((location, action) => {
        });
    }

    render() {
        return (
             <div className="wrapper">
                 <div className="col-sm-8 col-sm-offset-2">
                     <Router history={history}>
                         <div>
                             <Route path="/login" component={Login}/>
                             <LoginRoute path="/todo" component={Todo}/>
                         </div>
                     </Router>
                 </div>
             </div>
        );
    }
}


export default (App);
