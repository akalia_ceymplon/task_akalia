import React, {Component} from 'react';
import TodoItems from "../list/list";
import {connect} from 'react-redux'
import Button from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {TaskActions} from '../../actions';
import '../../assets/task.css';

class Todo extends Component {
    componentWillMount() {
        this.loadRemoteTodos();
    }

    loadRemoteTodos() {
        this.props.dispatch(TaskActions.index());
    }

    constructor(props) {
        super(props);
        this.state = {
            items: this.props.items,
            error: false,
            is_edit: false,
            editValue: '',
            btnText: 'Create Task',
            editItem: [],
            value: '',
            disabled: false,
            menu: 'All'
        };
        this.addItem = this.addItem.bind(this);
        this.validate = this.validate.bind(this);
    }

    addItem(e) {
        e.preventDefault();
        if (this.state.value !== "") {
            var data = {
                note: this.state.value,
                description: this.state.value,
                status:'Created'
            }
            this.props.dispatch(TaskActions.create(JSON.stringify(data)))
            this.props.dispatch(TaskActions.index())
            this.setState({
                value: ''
            })
        } else {
            this.setState({
                error: true
            });
            this.handleCancel();
        }
    }


    validate(e) {
        if (!e.target) {
            this.setState({
                editValue: e
            });
            if (e.trim() === '') {
                this.setState({
                    disabled: true
                })
            } else {
                this.setState({
                    disabled: false
                })
            }
        } else {
            if (e.target.value.trim() !== "") {
                this.setState({
                    error: false,
                    value: e.target.value
                });
            } else {
                this.setState({
                    error: true
                });
            }
        }

    }


    render() {
        return (
            <div className="container" id='app'>
                <div className="center">
                    <TextField
                        hintText="enter the task"
                        onChange={this.validate}
                        value={this.state.value}
                    />
                    <Button onClick={this.addItem} backgroundColor="green">Create Task</Button>
                </div>
                <div>
                </div>
                <TodoItems
                    validate={this.validate}
                    propsData={this.state}
                    entries={this.props.items}/>
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.todos
    };
}


export default connect(mapStateToProps)(Todo);
