import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import '../../assets/auth.css';
import {connect} from 'react-redux';
import {UserActions} from '../../actions';

class Login extends Component {
constructor(props){
  super(props);
  this.state={
  username:'',
  password:''
}
  this.onSubmit = this.onSubmit.bind(this)
}


onSubmit(e){
  e.preventDefault();
  const {username, password} = this.state;
  const {dispatch} = this.props;
        if (username && password) {
            dispatch(UserActions.login(username, password));
        }
}
render() {
    return (
          <form name="form" onSubmit={this.onSubmit} className="form-signin">
              <p>Enter your email address and passphrase to authenticate:</p>
              <TextField
                hintText="Enter your email"
                onChange = {(event,value) => this.setState({username:value})}
                />
              <br/>
                <TextField
                  type="password"
                  hintText="Enter your Password"
                  onChange = {(event,value) => this.setState({password:value})}
                  />
                <br/>
              <button >Login</button>
           </form>
    );
  }
}

const mapStateToProps = () => ({

});



export default connect(mapStateToProps)(Login);
