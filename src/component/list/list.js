import React, { Component } from "react";
import FlipMove from 'react-flip-move';
import { Flex, Box } from 'reflexbox';


class TodoItems extends Component {
  constructor(props, context) {
    super(props, context);

    this.createTasks = this.createTasks.bind(this);
  }

  createTasks(item) {
      return(
        <div className="scrollContainer">
                <Flex>
                    <Box px={2} w={2/2} style={{
                        color: '#fff',
                        backgroundColor: 'grey',
                        borderRadius: 6,
                        mixBlendMode: 'multiply'
                    }}>
                            <span  key={item.id}>{item.note}</span>
                    </Box>
                </Flex>
        </div>
      )
  }

  render() {
    var todoEntries = this.props.entries;
    if(typeof(todoEntries) === 'object' && !this.props.entries.length){
      todoEntries = [];
    }
    var listItems = todoEntries.map(item => this.createTasks(item));
    if(listItems.length){
      return (
        <div className="container">
         <FlipMove duration={250} easing="ease-out">
            {listItems}
          </FlipMove>
        </div>
      );
    }else{
      return (
        <div>
           <span>No Tasks Here</span>
        </div>
      );
    }
  }
};

export default TodoItems;
